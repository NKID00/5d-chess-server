# 5D Chess Server

[![Pipeline Status](https://gitlab.com/5d-chess/5d-chess-renderer/badges/master/pipeline.svg)](https://gitlab.com/%{project_path}/-/commits/master)

Open source implementation of '5D Chess With Multiverse Time Travel' as a REST style API server with ELO rankings.

Official server instance is https://server.chessin5d.net.

## Documentation

Documentation is available [here](https://5d-chess.gitlab.io/5d-chess-server)!

To run local copy, use the command `npm run docs-start`.

## Copyright

All source code is released under AGPL v3.0 (license can be found under the `LICENSE` file).

Any addition copyrightable material not covered under AGPL v3.0 is released under CC BY-SA v3.0.

*Note: For `defaultConfig.json`, since the software cannot be run without the file, it is considered "corresponding source" in section 1 in the AGPL v3.0. This means any modification is subject to the AGPL v3.0 license. For `config.json`, that file is not considered "corresponding source" (in the eyes of the author), so any modification is not subject to the AGPL v3.0 license. Please use `config.json` instead of `defaultConfig.json` in production environments to protect private secrets.*
