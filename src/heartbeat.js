const config = require('./config');
const chessInterface = require('./chessInterface');
const userInterface = require('./userInterface');
const logInterface = require('./logInterface');
const room = require('./xmpp/room');

var interval = null;

const heartbeatFunc = async () => {
  var sessions = await chessInterface.getSessions();
  for(var i = 0;i < sessions.length;i++) {
    if(!sessions[i].started && sessions[i].startDate + config.startDeadline <= Date.now()) {
      //Reached start deadline
      try {
        await chessInterface.removeSession(sessions[i].id, true);
      }
      catch(err) {}
    }
    if(sessions[i].startDate + config.deadline <= Date.now()) {
      //Reached deadline
      try {
        await chessInterface.removeSession(sessions[i].id, true);
      }
      catch(err) {}
    }
    else if(sessions[i].ended && sessions[i].archiveDate <= Date.now()) {
      //Reached archive
      try {
        await chessInterface.removeSession(sessions[i].id, true);
      }
      catch(err) {}
    }
  }
  //Keep rooms active
  if(config.xmpp.enable) {
    for(var i = 0;i < config.xmpp.defaultRooms.length;i++) {
      await room.addRoom(config.xmpp.defaultRooms[i]);
    }
  }
  var users = await userInterface.getUsers();
  for(var i = 0;i < users.length;i++) {
    //Create needed xmpp users
    await userInterface.setXmppPass(users[i].username, users[i]);
    //Create avatars if needed
    await userInterface.setRandomAvatar(users[i].username, users[i]);
  }

  await logInterface.cleanLog();
};

exports.init = async () => {
  if(interval === null) {
    await heartbeatFunc();
    interval = setInterval(heartbeatFunc, config.heartbeat);
  }
};

exports.clear = () => {
  if(interval !== null) {
    clearInterval(interval);
  }
};
