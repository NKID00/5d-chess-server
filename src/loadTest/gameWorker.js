const { expose } = require('threads/worker');

const axios = require('axios');

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds));
};
const singleGame = async (url) => {
  var hostUsername = `loadtestuser_${Math.ceil(Math.random() * 9999)}`;
  var clientUsername = `loadtestuser_${Math.ceil(Math.random() * 9999)}`;
  var hostJWT = (await axios.post(`${url}/register`, {
    username: hostUsername,
    password: hostUsername
  })).data;
  console.log(`Host: ${hostUsername} JWT: ${hostJWT}`);
  await sleep(6000);
  var clientJWT = (await axios.post(`${url}/register`, {
    username: clientUsername,
    password: clientUsername
  })).data;
  console.log(`Client: ${clientUsername} JWT: ${clientJWT}`);
  await sleep(6000);
  
  var sessionId = (await axios.post(`${url}/sessions/new`, {
    player: 'white'
  }, { headers: { 'Authorization': hostJWT } })).data.id;
  console.log(`Creating session: ${sessionId}`);
  await sleep(1000);
  
  await axios.post(`${url}/sessions/${sessionId}/addUser`, {
    username: clientUsername
  }, { headers: { 'Authorization': hostJWT } });
  console.log(`Adding user to session: ${sessionId}`);
  await sleep(1000);

  await axios.post(`${url}/sessions/${sessionId}/ready`, {}, { headers: { 'Authorization': clientJWT } });
  console.log(`Non-host user ready for session: ${sessionId}`);
  await sleep(1000);
  
  await axios.post(`${url}/sessions/${sessionId}/start`, {}, { headers: { 'Authorization': hostJWT } });
  console.log(`Starting session: ${sessionId}`);
  await sleep(1000);

  await axios.post(`${url}/sessions/${sessionId}/move`, {
    "promotion": null,
    "enPassant": null,
    "castling": null,
    "start": {
      "timeline": 0,
      "turn": 1,
      "player": "white",
      "coordinate": "e2",
      "rank": 2,
      "file": 5
    },
    "end": {
      "timeline": 0,
      "turn": 1,
      "player": "white",
      "coordinate": "e3",
      "rank": 3,
      "file": 5
    },
    "player": "white"
  }, { headers: { 'Authorization': hostJWT } });
  console.log(`Host: ${hostUsername} playing move in session: ${sessionId}`);
  await sleep(1000);
  
  await axios.post(`${url}/sessions/${sessionId}/submit`, {}, { headers: { 'Authorization': hostJWT } });
  console.log(`Host: ${hostUsername} submitting in session: ${sessionId}`);
  await sleep(1000);
  
  await axios.post(`${url}/sessions/${sessionId}/move`, {
    "promotion": null,
    "enPassant": null,
    "castling": null,
    "start": {
      "timeline": 0,
      "turn": 1,
      "player": "black",
      "coordinate": "f7",
      "rank": 7,
      "file": 6
    },
    "end": {
      "timeline": 0,
      "turn": 1,
      "player": "black",
      "coordinate": "f6",
      "rank": 6,
      "file": 6
    },
    "player": "black"
  }, { headers: { 'Authorization': clientJWT } });
  console.log(`Client: ${clientUsername} playing move in session: ${sessionId}`);
  await sleep(1000);
  
  await axios.post(`${url}/sessions/${sessionId}/submit`, {}, { headers: { 'Authorization': clientJWT } });
  console.log(`Client: ${clientUsername} submitting in session: ${sessionId}`);
  await sleep(1000);

  await axios.post(`${url}/sessions/${sessionId}/move`, {
    "promotion":null,"enPassant":null,"castling":null,"start":{"timeline":0,"turn":2,"player":"white","coordinate":"d1","rank":1,"file":4},"end":{"timeline":0,"turn":2,"player":"white","coordinate":"f3","rank":3,"file":6},"player":"white"
  }, { headers: { 'Authorization': hostJWT } });
  console.log(`Host: ${hostUsername} playing move in session: ${sessionId}`);
  await sleep(1000);
  
  await axios.post(`${url}/sessions/${sessionId}/submit`, {}, { headers: { 'Authorization': hostJWT } });
  console.log(`Host: ${hostUsername} submitting in session: ${sessionId}`);
  await sleep(1000);
  
  await axios.post(`${url}/sessions/${sessionId}/move`, {
    "promotion":null,"enPassant":null,"castling":null,"start":{"timeline":0,"turn":2,"player":"black","coordinate":"d7","rank":7,"file":4},"end":{"timeline":0,"turn":2,"player":"black","coordinate":"d6","rank":6,"file":4},"player":"black"
  }, { headers: { 'Authorization': clientJWT } });
  console.log(`Client: ${clientUsername} playing move in session: ${sessionId}`);
  await sleep(1000);
  
  await axios.post(`${url}/sessions/${sessionId}/submit`, {}, { headers: { 'Authorization': clientJWT } });
  console.log(`Client: ${clientUsername} submitting in session: ${sessionId}`);
  await sleep(1000);

  await axios.post(`${url}/sessions/${sessionId}/move`, {
    "promotion":null,"enPassant":null,"castling":null,"start":{"timeline":0,"turn":3,"player":"white","coordinate":"f3","rank":3,"file":6},"end":{"timeline":0,"turn":3,"player":"white","coordinate":"h5","rank":5,"file":8},"player":"white"
  }, { headers: { 'Authorization': hostJWT } });
  console.log(`Host: ${hostUsername} playing move in session: ${sessionId}`);
  await sleep(1000);
  
  await axios.post(`${url}/sessions/${sessionId}/submit`, {}, { headers: { 'Authorization': hostJWT } });
  console.log(`Host: ${hostUsername} submitting in session: ${sessionId}`);
  await sleep(1000);
  return null;
};

expose({
  singleGame: singleGame,
  multipleGames: async (url, num) => {
    var p = [];
    for(var i = 0;i < num;i++) {
      p.push(singleGame(url));
      await sleep(2000);
    }
    await (Promise.all(p));
  }
});
