const config = require('./config');
const mailer = require('nodemailer');
const smtp = require('nodemailer-smtp-transport');

const mail = mailer.createTransport(
  smtp(config.emailOptions)
);

exports.sendRecoveryCode = async (email, recoverCode) => {
  var url = `${config.emailRecovery.recoverUrl}${recoverCode}`;
  await mail.sendMail({
    from: config.emailRecovery.address,
    to: email,
    subject: config.emailRecovery.subject,
    html: `<h4>${config.emailRecovery.title}</h4>
<p>
A password recovery code has been generated. 
</p>
<p>
Follow this link to recover: <a href=${url}>${url}</a>
</p>
<p>
Recovery code is: ${recoverCode}
</p>
<p>
Ignore this email if you did not make this request.
</p>`
  });
}