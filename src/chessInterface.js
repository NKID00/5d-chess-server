const { spawn, Thread, Worker } = require('threads');
const ChessClock = require('5d-chess-clock');
const gameInterface = require('./gameInterface');
const logInterface = require('./logInterface');
const rankedFuncs = require('./ranked');
const config = require('./config');
var collections = require('./db').init();
const { v5: uuidv5 } = require('uuid');
const room = require('./xmpp/room');
var chess = null;

const initWorker = async () => {
  if(chess === null) {
    chess = await spawn(new Worker('./chessWorker'));
  }
};

const onEnd = async (ses) => {
  if(ses.ended) {
    if(ses.black !== ses.white) {
      await initWorker();
      var exportData = await chess.exportSession(ses);
      await gameInterface.newGame(exportData);
    }
  }
};

const checkTime = async (ses) => {
  if(ses.timed && !ses.ended && ses.started) {
    let chessClock = new ChessClock();
    chessClock.state(ses.timed);
    ses.timed = chessClock.state();
    if(ses.player === 'white') {
      if(ses.timed.whiteDurationLeft <= 0 && ses.timed.whiteDelayLeft <= 0 && ses.timed.lastUpdate > 0) {
        ses.timed.whiteDurationLeft = 0;
        ses.winner = 'black';
        ses.winCause = 'time';
        ses.ended = true;
        ses.endDate = Date.now();
        ses.archiveDate = Date.now() + config.archive;
      }
    }
    else {
      if(ses.timed.blackDurationLeft <= 0 && ses.timed.blackDelayLeft <= 0 && ses.timed.lastUpdate > 0) {
        ses.timed.blackDurationLeft = 0;
        ses.winner = 'white';
        ses.winCause = 'time';
        ses.ended = true;
        ses.endDate = Date.now();
        ses.archiveDate = Date.now() + config.archive;
      }
    }
    await collections.sessions.update({ id: ses.id }, { $set: ses });
  }
};

exports.authHost = async (id, username) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    return (username === existingSession.host);
  }
  else {
    throw 'Session not found!';
  }
};

exports.authNonHost = async (id, username) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    //Return true if this is self spectating mode and user is host
    if(existingSession.black === existingSession.host && existingSession.black === existingSession.white) { return username === existingSession.host; }
    return (username !== existingSession.host) && (username === existingSession.black || username === existingSession.white);
  }
  else {
    throw 'Session not found!';
  }
};

exports.authPlayer = async (id, username) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    return ((username === existingSession.black && existingSession.player === 'black') || (username === existingSession.white && existingSession.player === 'white'));
  }
  else {
    throw 'Session not found!';
  }
};

exports.newSession = async (username, player = 'white', variant = 'standard', ranked = false, format = null) => {
  await initWorker();
  var newSession = await chess.createSession(variant);
  let formatString = null;
  let clockState = null;
  if(format !== null) {
    let chessClock = new ChessClock();
    chessClock.reset(format);
    formatString = chessClock.format;
    for(let formatObj of chessClock.formats) {
      if(formatObj.name === formatString) { 
        formatString = formatObj.shortName;
      };
    }
    clockState = chessClock.state();
  }
  if(player === 'random') {
    player = Math.random() > 0.5 ? 'white' : 'black';
  }
  if(ranked) {
    rankedFuncs.validRanked(variant, format);
  }
  newSession = await chess.updateSession(newSession, {
    host: username,
    white: player === 'white' ? username : null,
    black: player !== 'white' ? username : null,
    ranked: ranked,
    timed: clockState,
    format: formatString
  });
  newSession.id = uuidv5(newSession.host + newSession.variant + newSession.format + (Date.now()), '97d87037-c436-4f77-8858-d0ab10d5085e');
  var ret = (await collections.sessions.insert(newSession));
  var copy = Object.assign({}, ret);
  delete copy.board;
  delete copy.actionHistory;
  delete copy.moveBuffer;
  delete copy.timed;
  await room.addRoom(`session-play-${newSession.id}`);
  await room.addRoom(`session-observe-${newSession.id}`);
  logInterface.addLogEntry('NewSession', 'Info', newSession);
  return ret;
};

exports.updateSession = async (id, player, variant, ranked, format) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(existingSession.ready) {
      throw 'Non-host player already signaled they are ready, cannot change session.';
    }
    if(!existingSession.started) {
      let formatString = null;
      let clockState = null;
      if(format !== null) {
        let chessClock = new ChessClock();
        chessClock.reset(format);
        formatString = chessClock.format;
        for(let formatObj of chessClock.formats) {
          if(formatObj.name === formatString) { 
            formatString = formatObj.shortName;
          };
        }
        clockState = chessClock.state();
      }
      if(player === 'random') {
        player = Math.random() > 0.5 ? 'white' : 'black';
      }
      if(ranked) {
        rankedFuncs.validRanked(variant, format);
      }
      existingSession = await chess.updateSession(existingSession, {
        white: typeof player !== 'string' ?
          null 
        : player === 'white' ? 
          existingSession.host
        :
          existingSession.white === existingSession.host ?
            existingSession.black
          :
            null,
        black: typeof player !== 'string' ?
          null 
        : player !== 'white' ? 
          existingSession.host
        :
          existingSession.black === existingSession.host ?
            existingSession.white
          :
            null,
        ranked: ranked,
        timed: clockState,
        format: formatString,
        variant: variant
      });
      delete existingSession._id;
      await collections.sessions.update({ id: id }, { $set: existingSession });
      logInterface.addLogEntry('UpdateSession', 'Info', existingSession);
      return existingSession;
    }
    else {
      throw 'Session started already!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.removeSession = async (id, force = false) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(!existingSession.started || force) {
      logInterface.addLogEntry('RemoveSession', 'Info', existingSession);
      await collections.sessions.remove({ id: id });
      await room.removeRoom(`session-play-${id}`);
      await room.removeRoom(`session-observe-${id}`);
      return null;
    }
    else {
      throw 'Session started already!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.addUser = async (id, username) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(!existingSession.started) {
      if(existingSession.white !== null) {
        existingSession.black = username;
      }
      else {
        existingSession.white = username;
      }
      delete existingSession._id;
      await collections.sessions.update({ id: id }, { $set: existingSession });
      logInterface.addLogEntry('AddUserSession', 'Info', existingSession);
      return existingSession;
    }
    else {
      throw 'Session started already!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.requestJoin = async (id, username) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(!existingSession.started) {
      if(existingSession.requestJoin.includes(username)) {
        throw 'User already requested to join session';
      }
      existingSession.requestJoin.push(username);
      delete existingSession._id;
      await collections.sessions.update({ id: id }, { $set: existingSession });
      logInterface.addLogEntry('RequestJoinSession', 'Info', existingSession);
      return existingSession;
    }
    else {
      throw 'Session started already!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionReady = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(!existingSession.started) {
      existingSession.ready = true;
      delete existingSession._id;
      await collections.sessions.update({ id: id }, { $set: existingSession });
      logInterface.addLogEntry('ReadySession', 'Info', existingSession);
      return existingSession;
    }
    else {
      throw 'Session started already!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionUnready = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(!existingSession.started) {
      existingSession.ready = false;
      delete existingSession._id;
      await collections.sessions.update({ id: id }, { $set: existingSession });
      logInterface.addLogEntry('UnreadySession', 'Info', existingSession);
      return existingSession;
    }
    else {
      throw 'Session started already!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionStart = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(!existingSession.started) {
      await initWorker();
      var ended = existingSession.ended;
      existingSession = await chess.startSession(existingSession);
      //Start session if timed
      if(existingSession.timed) {
        let chessClock = new ChessClock();
        chessClock.state(existingSession.timed);
        chessClock.start(existingSession.board);
        existingSession.timed = chessClock.state();
        delete existingSession._id;
        await collections.sessions.update({ id: id }, { $set: existingSession });
      }
      if(!ended) { onEnd(existingSession); }
      delete existingSession._id;
      await collections.sessions.update({ id: id }, { $set: existingSession });
      logInterface.addLogEntry('StartSession', 'Info', existingSession);
      return existingSession;
    }
    else {
      throw 'Session started already!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionMove = async (id, move) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(existingSession.started) {
      if(existingSession.processing) {
        return existingSession;
      }
      await initWorker();
      var ended = existingSession.ended;
      existingSession = await chess.moveSession(existingSession, move);
      delete existingSession._id;
      await collections.sessions.update({ id: id }, { $set: existingSession });
      await checkTime(existingSession);
      if(!ended) { onEnd(existingSession); }
      logInterface.addLogEntry('MoveSession', 'Info', existingSession);
      return existingSession;
    }
    else {
      throw 'Session not started!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionUndo = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(existingSession.started) {
      if(existingSession.processing) {
        return existingSession;
      }
      await initWorker();
      var ended = existingSession.ended;
      existingSession = await chess.undoSession(existingSession);
      delete existingSession._id;
      await collections.sessions.update({ id: id }, { $set: existingSession });
      await checkTime(existingSession);
      if(!ended) { onEnd(existingSession); }
      logInterface.addLogEntry('UndoSession', 'Info', existingSession);
      return existingSession;
    }
    else {
      throw 'Session not started!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionSubmit = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(existingSession.started) {
      if(existingSession.processing) {
        return existingSession;
      }
      //Pause session if timed
      if(existingSession.timed) {
        let chessClock = new ChessClock();
        chessClock.state(existingSession.timed);
        chessClock.stop();
        existingSession.timed = chessClock.state();
        await collections.sessions.update({ id: id }, { $set: existingSession });
      }
      await collections.sessions.update({ id: id }, { $set: { processing: true } });
      await initWorker();
      var ended = existingSession.ended;
      existingSession = await chess.submitSession(existingSession);
      delete existingSession._id;
      await collections.sessions.update({ id: id }, { $set: existingSession });
      await checkTime(existingSession);
      if(!ended) { onEnd(existingSession); }
      await collections.sessions.update({ id: id }, { $set: { processing: false } });
      //Resume session if timed
      if(existingSession.timed) {
        let chessClock = new ChessClock();
        chessClock.state(existingSession.timed);
        chessClock.start();
        chessClock.switch(existingSession.player, existingSession.board);
        existingSession.timed = chessClock.state();
        await collections.sessions.update({ id: id }, { $set: existingSession });
      }
      logInterface.addLogEntry('SubmitSession', 'Info', existingSession);
      return existingSession;
    }
    else {
      throw 'Session not started!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionForfeit = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(existingSession.started) {
      if(existingSession.processing) {
        return existingSession;
      }
      await initWorker();
      var ended = existingSession.ended;
      existingSession = await chess.forfeitSession(existingSession);
      delete existingSession._id;
      await collections.sessions.update({ id: id }, { $set: existingSession });
      await checkTime(existingSession);
      if(!ended) { onEnd(existingSession); }
      logInterface.addLogEntry('ForfeitSession', 'Info', existingSession);
      return existingSession;
    }
    else {
      throw 'Session not started!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionOfferDraw = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(existingSession.started) {
      if(existingSession.processing) {
        return existingSession;
      }
      await initWorker();
      var ended = existingSession.ended;
      delete existingSession._id;
      await collections.sessions.update({ id: id }, { $set: { offerDraw: true } });
      await checkTime(existingSession);
      if(!ended) { onEnd(existingSession); }
      logInterface.addLogEntry('OfferDrawSession', 'Info', existingSession);
      return existingSession;
    }
    else {
      throw 'Session not started!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionDraw = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(existingSession.started) {
      if(existingSession.processing) {
        return existingSession;
      }
      await initWorker();
      var ended = existingSession.ended;
      existingSession = await chess.drawSession(existingSession);
      delete existingSession._id;
      await collections.sessions.update({ id: id }, { $set: existingSession });
      await checkTime(existingSession);
      if(!ended) { onEnd(existingSession); }
      logInterface.addLogEntry('DrawSession', 'Info', existingSession);
      return existingSession;
    }
    else {
      throw 'Session not started!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.getSession = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    var ended = existingSession.ended;
    await checkTime(existingSession);
    if(!ended) { onEnd(existingSession); }
    delete existingSession._id;
    return existingSession;
  }
  else {
    throw 'Session not found!';
  }
};

exports.getSessions = async (query = {}, projection = {}, sort = {}, limit = 100, skip = 0) => {
  var res = [];
  var existingSessions = await (collections.sessions.findAsCursor(query, projection).sort(sort).limit(limit).skip(skip));
  if(!Array.isArray(existingSessions)) {
    existingSessions = await existingSessions.toArray();
  }
  for(var i = 0;i < existingSessions.length;i++) {
    var ended = existingSessions[i].ended;
    delete existingSessions[i]._id;
    await checkTime(existingSessions[i]);
    if(!ended) { onEnd(existingSessions[i]); }
    res.push(existingSessions[i]);
  }
  return res;
};
