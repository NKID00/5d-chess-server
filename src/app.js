const express = require('express');
const cors = require('cors');
const path = require('path');
const userRoutes = require('./userRoutes');
const chessRoutes = require('./chessRoutes');
const gameRoutes = require('./gameRoutes');
const rankingRoutes = require('./rankingRoutes');
const logRoutes = require('./logRoutes');
const rankedFuncs = require('./ranked');
const quickplayRoutes = require('./quickplayRoutes');
const auth = require('./auth');
const brute = require('./brute');
const config = require('./config');
const init = require('./init');

var app = express();

//Enable trust proxy if configured
if(config.enableTrustProxy) {
  app.enable('trust proxy');
}

//Allow CORS on all requests if configured
if(config.enableCors) {
  app.use(cors());
}

//JSON parse middleware
app.use(express.json());

//Admin Dashboard
app.get('/admin', brute.fast('admin'), (req, res) => {
  res.sendFile(path.join(__dirname, '/admin.html'));
});

//User Auth
app.post('/register', brute.slow('register'), userRoutes.register);
app.post('/login', brute.slow('login'), auth.softAuthRoute, userRoutes.login);
app.post('/recover', brute.slow('recover'), userRoutes.recover);
app.get('/refreshToken', brute.slow('refreshToken'), auth.authRoute, userRoutes.refresh);
app.get('/authCheck', brute.med('authCheck'), auth.authRoute, userRoutes.authCheck);
app.post('/recoverCode', brute.slow('recoverCode'), userRoutes.setRecoverCode);

//User Info
app.get('/users/:username', brute.med('users-username'), auth.softAuthRoute, userRoutes.getInfo);
app.get('/users', brute.med('g-users'), auth.softAuthRoute, userRoutes.getInfoQuery);
app.post('/users', brute.med('p-users'), auth.softAuthRoute, userRoutes.getInfoQuery);
app.post('/users/:username/update', brute.slow('users-username-update'), auth.authRoute, userRoutes.update);

//User Admin
app.post('/users/:username/ban', brute.slow('users-username-ban'), auth.authRoute, userRoutes.banUser);
app.post('/users/:username/moderator', brute.slow('users-username-moderator'), auth.authRoute, userRoutes.moderatorUser);
app.post('/users/:username/admin', brute.slow('users-username-admin'), auth.authRoute, userRoutes.adminUser);
app.get('/logs', brute.med('g-logs'), auth.authRoute, logRoutes.getInfoQuery);
app.post('/logs', brute.med('p-logs'), auth.authRoute, logRoutes.getInfoQuery);

//Session
app.get('/sessions/:id', brute.fast('sessions-id'), auth.softAuthRoute, chessRoutes.getInfo);
app.get('/sessions', brute.med('g-sessions'), auth.softAuthRoute, chessRoutes.getInfoQuery);
app.post('/sessions', brute.med('p-sessions'), auth.softAuthRoute, chessRoutes.getInfoQuery);
app.post('/sessions/new', brute.med('sessions-new'), auth.authRoute, chessRoutes.new);
app.post('/sessions/update', brute.med('sessions-update'), auth.authRoute, chessRoutes.update);
app.post('/sessions/:id/remove', brute.med('sessions-id-remove'), auth.authRoute, chessRoutes.remove);
app.post('/sessions/:id/addUser', brute.med('sessions-id-addUser'), auth.authRoute, chessRoutes.addUser);
app.post('/sessions/:id/requestJoin', brute.med('sessions-id-requestJoin'), auth.authRoute, chessRoutes.requestJoin);
app.post('/sessions/:id/ready', brute.med('sessions-id-ready'), auth.authRoute, chessRoutes.ready);
app.post('/sessions/:id/unready', brute.med('sessions-id-unready'), auth.authRoute, chessRoutes.unready);
app.post('/sessions/:id/start', brute.med('sessions-id-start'), auth.authRoute, chessRoutes.start);
app.post('/sessions/:id/move', brute.fast('sessions-id-move'), auth.authRoute, chessRoutes.move);
app.post('/sessions/:id/undo', brute.fast('sessions-id-undo'), auth.authRoute, chessRoutes.undo);
app.post('/sessions/:id/submit', brute.fast('sessions-id-submit'), auth.authRoute, chessRoutes.submit);
app.post('/sessions/:id/forfeit', brute.med('sessions-id-forfeit'), auth.authRoute, chessRoutes.forfeit);
app.post('/sessions/:id/draw', brute.med('sessions-id-draw'), auth.authRoute, chessRoutes.draw);

//Game
app.get('/games/:id', brute.med('games-id'), auth.softAuthRoute, gameRoutes.getInfo);
app.get('/games', brute.med('g-games'), auth.softAuthRoute, gameRoutes.getInfoQuery);
app.post('/games', brute.med('p-games'), auth.softAuthRoute, gameRoutes.getInfoQuery);

//Ranking
app.get('/rankings/:id', brute.med('rankings-id'), auth.softAuthRoute, rankingRoutes.getInfo);
app.get('/rankings', brute.med('g-rankings'), auth.softAuthRoute, rankingRoutes.getInfoQuery);
app.post('/rankings', brute.med('p-rankings'), auth.softAuthRoute, rankingRoutes.getInfoQuery);

//Ranked
app.get('/ranked/variants', brute.med('ranked-variants'), auth.softAuthRoute, rankedFuncs.availableVariants);
app.get('/ranked/formats', brute.med('ranked-formats'), auth.softAuthRoute, rankedFuncs.availableFormats);

//Quickplay
app.get('/quickplay/queue', brute.fast('g-quickplay-queue'), auth.authRoute, quickplayRoutes.getInfo);
app.post('/quickplay/queue', brute.med('p-quickplay-queue'), auth.authRoute, quickplayRoutes.queue);
app.get('/quickplay/cancel', brute.med('quickplay-cancel'), auth.authRoute, quickplayRoutes.cancel);
app.get('/quickplay/confirm', brute.med('quickplay-confirm'), auth.authRoute, quickplayRoutes.confirm);
app.get('/quickplay', brute.med('quickplay'), auth.softAuthRoute, quickplayRoutes.getStats);

//Xmpp
app.get('/xmpp', brute.med('xmpp'), auth.authRoute, userRoutes.getXmpp);

//General
app.get('/message', brute.med('message'), (req, res) => {
  res.status(200).send(config.motdString);
});
app.get('/', brute.med('root'), (req, res) => {
  res.status(200).send(config.gatewayString);
});

//GCP App Engine warmup route
app.get('/_ah/warmup', async (req, res) => { res.status(200).end(); });

//Testing initialization route
app.get('/_init', async (req, res) => {
  await init.init();
  res.status(200).end();
});

module.exports = app;
