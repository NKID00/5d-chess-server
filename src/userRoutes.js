const userInterface = require('./userInterface');
const auth = require('./auth');
const config = require('./config');
const secret = require('./secret');
const countries = require('i18n-iso-countries');
const profanityFilter = require('leo-profanity');
const validator = require('validator');

exports.userTransform = (user, authed = false) => {
  return {
    avatar: user.avatar,
    username: user.username,
    fullname: user.fullname,
    email: user.showEmail || authed ? user.email : '',
    showEmail: user.showEmail,
    bio: user.bio,
    country: user.country,
    bot: user.bot,
    joinDate: user.joinDate,
    banDate: user.banDate,
    lastAuth: user.lastAuth ? user.lastAuth : 0,
    admin: user.admin ? user.admin : false,
    moderator: user.moderator ? user.moderator : false,
  };
};

//Common function to sanitize user update information
const userUpdate = async (data) => {
  var additionalInfo = {};
  var valid = true;
  var error = '';
  if(valid && typeof data.fullname !== 'undefined') {
    if(typeof data.fullname !== 'string') {
      error = 'Fullname field is not string!';
      valid = false;
    }
    else if(profanityFilter.check(data.fullname)) {
      error = 'Fullname field contains profanity!';
      valid = false;
    }
    else {
      additionalInfo.fullname = data.fullname.substr(0, 100);
    }
  }
  if(valid && typeof data.email !== 'undefined') {
    if(typeof data.email !== 'string') {
      error = 'Email field is not string!';
      valid = false;
    }
    else if(!validator.isEmail(data.email)) {
      error = 'Email field is not a valid email!';
      valid = false;
    }
    else {
      additionalInfo.email = data.email;
    }
  }
  if(valid && typeof data.showEmail !== 'undefined') {
    if(typeof data.showEmail !== 'boolean') {
      error = 'Show Email field is not boolean!';
      valid = false;
    }
    else {
      additionalInfo.showEmail = data.showEmail;
    }
  }
  if(valid && typeof data.bio !== 'undefined') {
    if(typeof data.bio !== 'string') {
      error = 'Bio field is not string!';
      valid = false;
    }
    else if(profanityFilter.check(data.bio)) {
      error = 'Bio field contains profanity!';
      valid = false;
    }
    else {
      additionalInfo.bio = data.bio.substr(0, 500);
    }
  }
  if(valid && typeof data.country !== 'undefined') {
    if(typeof data.country !== 'string') {
      error = 'Country field is not string!';
      valid = false;
    }
    else {
      if(typeof countries.getAlpha3Codes()[data.country] !== 'undefined') {
        additionalInfo.country = data.country;
      }
      else {
        error = 'Country field is not ISO 3166-1 Alpha-3 compliant! (List here: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3#Officially_assigned_code_elements)';
        valid = false;
      }
    }
  }
  if(valid && typeof data.avatar !== 'undefined') {
    if(typeof data.avatar !== 'string') {
      error = 'Avatar field is not string!';
      valid = false;
    }
    else {
      additionalInfo.avatar = data.avatar;
    }
  }
  if(valid && typeof data.token === 'string') {
    if(data.token === (await secret.getNonbot())) {
       additionalInfo.bot = false;
    }
  }
  return {
    error: error,
    valid: valid,
    additionalInfo: additionalInfo
  }
}

exports.register = async (req, res) => {
  var data = req.body;
  var additionalInfo = {};
  var valid = true;
  var error = '';
  if(typeof data.username !== 'string') {
    error = 'Username is not string!';
    valid = false;
  }
  else {
    var regex = /^[a-z\d\-_]+$/;
    var match = data.username.match(regex);
    if(match === null || match[0] !== data.username) {
      error = 'Username is invalid! Valid characters are a-z, 0-9, _ and - characters.';
      valid = false;
    }
    else if(profanityFilter.check(data.username)) {
      error = 'Username contains profanity!';
      valid = false;
    }
    else if(config.usernameBlacklist.includes(data.username)) {
      error = 'Username is blacklisted. Pick another username.';
      valid = false;
    }
  }
  if(valid && typeof data.password !== 'string') {
    error = 'Password is not string!';
    valid = false;
  }
  if(valid) {
    var updateData = (await userUpdate(data));
    error = updateData.error;
    valid = updateData.valid;
    additionalInfo = updateData.additionalInfo;
  }
  if(valid) {
    try {
      await userInterface.registerUser(data.username.substr(0, 100), data.password.substr(0, 100), additionalInfo);
      var token = (await auth.tokenSign(data.username.substr(0, 100)));
      res.status(200).send(token);
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.login = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof data.username !== 'string') {
    error = 'Username is not string!';
    valid = false;
  }
  else {
    var regex = /^[a-z\d\-_]+$/;
    var match = data.username.match(regex);
    if(match === null || match[0] !== data.username) {
      error = 'Username is invalid! Valid characters are a-z, 0-9, _ and - characters.';
      valid = false;
    }
  }
  if(valid && typeof data.password !== 'string') {
    error = 'Password is not string!';
    valid = false;
  }
  if(valid) {
    try {
      var isAuth = await userInterface.authUser(data.username, data.password);
      if(isAuth) {
        var token = (await auth.tokenSign(data.username));
        res.status(200).send(token);
      }
      else {
        if(!res.headersSent) { res.status(403).send({ error: 'Username or Password do not match!' }); }
      }
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.refresh = async (req, res) => {
  if(req.username) {
    try {
      var token = (await auth.tokenSign(req.username));
      res.status(200).send(token);
    }
    catch(err) {
      res.status(500).send({ error: err });
    }
  }
};

exports.authCheck = async (req, res) => {
  if(req.username) {
    try {
      res.status(200).end();
    }
    catch(err) {
      res.status(500).send({ error: err });
    }
  }
};

exports.update = async (req, res) => {
  if(req.username === req.params.username || (await userInterface.getUser(req.username)).admin) {
    var data = req.body;
    var additionalInfo = {};
    var valid = true;
    var error = '';
    if(valid) {
      var updateData = (await userUpdate(data));
      error = updateData.error;
      valid = updateData.valid;
      additionalInfo = updateData.additionalInfo;
    }
    if(valid) {
      try {
        var user = await userInterface.updateUser(req.username, additionalInfo);
        res.status(200).send(this.userTransform(user, true));
      }
      catch(err) {
        error = typeof err === 'string' ? err : err.message;
        valid = false;
      }
    }
    if(!valid) {
      if(!res.headersSent) { res.status(403).send({ error: error }); }
    }
  }
  else {
    res.status(403).send({ error: 'Requested user is not same as requesting user!' });
  }
};

exports.getInfo = async (req, res) => {
  try {
    var user = (await userInterface.getUser(req.params.username));
    res.status(200).send(this.userTransform(user, req.username === user.username));
  }
  catch(err) {
    res.status(500).send({ error: typeof err === 'string' ? err : err.message });
  }
};

exports.getInfoQuery = async (req, res) => {
  try {
    if(typeof req.body !== 'object') {
      req.body = {
        query: {},
        projection: {},
        sort: {},
        limit: 100,
        skip: 0
      };
    }
    if(typeof req.body.query !== 'object') { req.body.query = {}; }
    if(typeof req.body.projection !== 'object') { req.body.projection = {}; }
    if(typeof req.body.sort !== 'object') { req.body.sort = {}; }
    if(typeof req.body.limit !== 'number') { req.body.limit = 100; }
    if(typeof req.body.skip !== 'number') { req.body.skip = 0; }
    var users = (await userInterface.getUsers(req.body.query, req.body.projection, req.body.sort, req.body.limit, req.body.skip)).map(e => this.userTransform(e, req.username === e.username));
    res.status(200).send(users);
  }
  catch(err) {
    res.status(500).send({ error: typeof err === 'string' ? err : err.message });
  }
};

exports.getXmpp = async (req, res) => {
  try {
    var user = (await userInterface.getUser(req.username));
    if(!config.xmpp.enable || typeof user.xmppPass !== 'string') {
      res.status(200).send({});
    }
    else {
      res.status(200).send({
        domain: config.xmpp.domain,
        muc: config.xmpp.muc,
        username: `${user.username}@${config.xmpp.domain}`,
        password: user.xmppPass,
        defaultRooms: config.xmpp.defaultRooms.map(e => `${e}@${config.xmpp.muc}`)
      });
    }
  }
  catch(err) {
    res.status(500).send({ error: typeof err === 'string' ? err : err.message });
  }
};

exports.setRecoverCode = async (req, res) => {
  try {
    if(typeof req.body.email !== 'string') {
      throw 'Email is not string!';
    }
    else if(!validator.isEmail(req.body.email)) {
      throw 'Email is not a valid email!';
    }
    var user = (await userInterface.setRecoverCode(req.body.email));
    res.status(200).end();
  }
  catch(err) {
    res.status(500).send({ error: typeof err === 'string' ? err : err.message });
  }
};

exports.recover = async (req, res) => {
  try {
    if(typeof req.body.email !== 'string') {
      throw 'Email is not string!';
    }
    else if(!validator.isEmail(req.body.email)) {
      throw 'Email is not a valid email!';
    }
    if(typeof req.body.recoverCode !== 'string') {
      throw 'Recovery code is not string!';
    }
    if(typeof req.body.password !== 'string') {
      throw 'Password is not string!';
    }
    var user = (await userInterface.recoverUser(req.body.email, req.body.recoverCode, req.body.password));
    res.status(200).end();
  }
  catch(err) {
    res.status(500).send({ error: typeof err === 'string' ? err : err.message });
  }
};

exports.banUser = async (req, res) => {
  let currentUser = await userInterface.getUser(req.username);
  if(currentUser.moderator || currentUser.admin) {
    try {
      if(typeof req.params.username !== 'string') {
        throw 'Username is not string!';
      }
      if(typeof req.body.banDate !== 'number' && typeof req.body.banDate !== 'boolean') {
        throw 'Ban Date is not valid!';
      }
      let user = await userInterface.banUser(req.params.username, req.body.banDate);
      res.status(200).send(this.userTransform(user, false));
    }
    catch(err) {
      res.status(500).send({ error: typeof err === 'string' ? err : err.message });
    }
  }
  else {
    res.status(403).send({ error: 'Requesting user is not moderator or admin!' });
  }
};

exports.moderatorUser = async (req, res) => {
  let currentUser = await userInterface.getUser(req.username);
  if(currentUser.moderator || currentUser.admin) {
    try {
      if(typeof req.params.username !== 'string') {
        throw 'Username is not string!';
      }
      if(typeof req.body.moderator !== 'boolean') {
        throw 'Moderator is not valid!';
      }
      let user = await userInterface.updateUser(req.params.username, { moderator: req.body.moderator });
      res.status(200).send(this.userTransform(user, false));
    }
    catch(err) {
      res.status(500).send({ error: typeof err === 'string' ? err : err.message });
    }
  }
  else {
    res.status(403).send({ error: 'Requesting user is not moderator or admin!' });
  }
};

exports.adminUser = async (req, res) => {
  let currentUser = await userInterface.getUser(req.username);
  if(currentUser.admin) {
    try {
      if(typeof req.params.username !== 'string') {
        throw 'Username is not string!';
      }
      if(typeof req.body.admin !== 'boolean') {
        throw 'Admin is not valid!';
      }
      let user = await userInterface.updateUser(req.params.username, { admin: req.body.admin });
      res.status(200).send(this.userTransform(user, false));
    }
    catch(err) {
      res.status(500).send({ error: typeof err === 'string' ? err : err.message });
    }
  }
  else {
    res.status(403).send({ error: 'Requesting user is not admin!' });
  }
};
