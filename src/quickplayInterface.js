const config = require('./config');
var collections = require('./db').init();
const rankingInterface = require('./rankingInterface');
const chessInterface = require('./chessInterface');
const rankedFuncs = require('./ranked');
const { v5: uuidv5 } = require('uuid');

const match = async (id) => {
  let existingQueueTicket = await collections.quickplay.findOne({ id: id });
  if(existingQueueTicket !== null) {
    //Search for match if needed
    if(existingQueueTicket.matchId === null) {
      let ratingQuery = { $exists: true };
      if(typeof existingQueueTicket.minRating === 'number') {
        ratingQuery['$gte'] = existingQueueTicket.minRating;
      }
      if(typeof existingQueueTicket.maxRating === 'number') {
        ratingQuery['$lte'] = existingQueueTicket.maxRating;
      }
      let matches = (await (collections.quickplay.findAsCursor({
        $and: [
          { username: { $ne: existingQueueTicket.username } },
          { ranked: existingQueueTicket.ranked },
          { 
            $or: existingQueueTicket.variants.map(variant => {
              return { variants: variant };
            })
          },
          { 
            $or: existingQueueTicket.formats.map(format => {
              return { formats: format };
            })
          },
          { rating: ratingQuery },
          { sessionId: null },
          { matchId: null }
        ]
      }).sort({ date: 1 })));
      if(Array.isArray(matches) && matches.length > 0) {
        let matchingQueueTicket = matches[0];
        //Look for first variant and format match
        let variantMatch = null;
        let formatMatch = null;
        for(let variant of existingQueueTicket.variants) {
          if(matchingQueueTicket.variants.includes(variant) && variantMatch === null) {
            variantMatch = variant;
          }
        }
        for(let format of existingQueueTicket.formats) {
          if(matchingQueueTicket.formats.includes(format) && formatMatch === null) {
            formatMatch = format;
          }
        }
        if(variantMatch !== null && formatMatch !== null) {
          delete matchingQueueTicket._id;
          delete existingQueueTicket._id;
          existingQueueTicket.matchId = matchingQueueTicket.id;
          matchingQueueTicket.matchId = existingQueueTicket.id;
          existingQueueTicket.sessionId = '';
          matchingQueueTicket.sessionId = '';
          await collections.quickplay.update({ id: matchingQueueTicket.id, matchId: null }, { $set: matchingQueueTicket });
          await collections.quickplay.update({ id: existingQueueTicket.id }, { $set: existingQueueTicket });
          //Creating session
          let newSession = await chessInterface.newSession(existingQueueTicket.username, 'white', variantMatch, existingQueueTicket.ranked, formatMatch);
          existingQueueTicket.sessionId = newSession.id;
          matchingQueueTicket.sessionId = newSession.id;
          await collections.quickplay.update({ id: existingQueueTicket.id }, { $set: existingQueueTicket });
          await collections.quickplay.update({ id: matchingQueueTicket.id }, { $set: matchingQueueTicket });
          newSession.ready = true;
          if(existingQueueTicket.rating < matchingQueueTicket.rating) {
            newSession.white = existingQueueTicket.username;
            newSession.black = matchingQueueTicket.username;
          }
          else {
            newSession.white = matchingQueueTicket.username;
            newSession.black = existingQueueTicket.username;
          }
          await collections.sessions.update({ id: newSession.id }, { $set: newSession });
        }
      }
    }
    return existingQueueTicket;
  }
  else {
    throw 'Queue ticket not found!';
  }
};

exports.queue = async (username, ranked, variants, formats, minRating, maxRating) => {
  let existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null && !existingUser.bot) {
    let existingQueueTicket = await collections.quickplay.findOne({ username: username });
    if(existingQueueTicket === null) {
      let userRanking = await rankingInterface.getLatestRanking(username, 'overall', 'overall');
      let newDate = Date.now();
      let newQueueTicket = {
        id: uuidv5(username + newDate, '214fcb11-72b6-4d41-ab2e-797fc4ccae07'),
        username: username,
        ranked: ranked,
        variants: variants,
        formats: formats,
        rating: userRanking.rating,
        minRating: minRating,
        maxRating: maxRating,
        date: newDate,
        sessionId: null,
        matchId: null,
        confirm: false,
        confirmDate: -1,
      };
      await collections.quickplay.insert(newQueueTicket);
      try {
        return await match(newQueueTicket.id);
      }
      catch(err) {
        return newQueueTicket;
      }
    }
    else {
      throw 'Queue ticket already exists!';
    }
  }
  else {
    throw 'User not found!';
  }
};

exports.getInfo = async (username) => {
  let existingQueueTicket = await collections.quickplay.findOne({ username: username });
  if(existingQueueTicket !== null) {
    try {
      return await match(existingQueueTicket.id);
    }
    catch(err) {
      return existingQueueTicket;
    }
  }
  else {
    throw 'Queue ticket not found!';
  }
};

exports.cancel = async (username) => {
  let existingQueueTicket = await collections.quickplay.findOne({ username: username });
  if(existingQueueTicket !== null) {
    if(!existingQueueTicket.confirm || Date.now() - existingQueueTicket.confirmDate > 5*1000) {
      if(existingQueueTicket.sessionId !== null) {
        try {
          await chessInterface.removeSession(existingQueueTicket.sessionId, true);
        }
        catch(err) {}
      }
      if(existingQueueTicket.matchId !== null) {
        let matchingQueueTicket = await collections.quickplay.findOne({ id: existingQueueTicket.matchId });
        if(matchingQueueTicket !== null) {
          delete matchingQueueTicket._id;
          matchingQueueTicket.sessionId = null;
          matchingQueueTicket.matchId = null;
          matchingQueueTicket.confirm = false;
          await collections.quickplay.update({ id: matchingQueueTicket.id }, { $set: matchingQueueTicket });
        }
      }
      await collections.quickplay.remove({ id: existingQueueTicket.id });
    }
    else {
      throw 'Queue ticket already confirmed!';
    }
  }
};

exports.confirm = async (username) => {
  let existingQueueTicket = await collections.quickplay.findOne({ username: username });
  if(existingQueueTicket !== null) {
    if(!existingQueueTicket.confirm) {
      if(existingQueueTicket.matchId !== null) {
        let matchingQueueTicket = await collections.quickplay.findOne({ id: existingQueueTicket.matchId });
        if(matchingQueueTicket !== null && existingQueueTicket.sessionId !== null) {
          if(matchingQueueTicket.confirm) {
            await chessInterface.sessionStart(existingQueueTicket.sessionId);
            await collections.quickplay.remove({ id: existingQueueTicket.id });
            await collections.quickplay.remove({ id: matchingQueueTicket.id });
          }
          else {
            delete existingQueueTicket._id;
            existingQueueTicket.confirm = true;
            existingQueueTicket.confirmDate = Date.now();
            await collections.quickplay.update({ id: existingQueueTicket.id }, { $set: existingQueueTicket });
          }
        }
        else {
          throw 'Matching queue ticket not found!';
        }
      }
      else {
        throw 'Matching queue ticket not found!';
      }
    }
  }
  else {
    throw 'Queue ticket not found!';
  }
};

exports.getStats = async () => {
  let res = {
    totalPlayers: 0,
    variants: [],
    formats: []
  };
  res.totalPlayers = await collections.quickplay.count({ matchId: null });
  res.ranked = await collections.quickplay.count({ matchId: null, ranked: true });
  let variants = rankedFuncs.variants();
  for(let variant of variants) {
    res.variants.push({
      name: variant,
      totalPlayers: await collections.quickplay.count({ matchId: null, variants: variant }),
      ranked: await collections.quickplay.count({ matchId: null, variants: variant, ranked: true })
    });
  }
  let formats = rankedFuncs.formats();
  for(let format of formats) {
    res.formats.push({
      name: format,
      totalPlayers: await collections.quickplay.count({ matchId: null, formats: format }),
      ranked: await collections.quickplay.count({ matchId: null, formats: format, ranked: true })
    });
  }
  return res;
};