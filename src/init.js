const userInterface = require('./userInterface');
const secret = require('./secret');
const initXmpp = require('./xmpp/xmpp');
const room = require('./xmpp/room');
const config = require('./config');
const fs = require('fs');

exports.init = async () => {
  try {
    try {
      var adminPass = await secret.getAdmin();
      await userInterface.registerUser(config.defaultAdminUsername, adminPass, { admin: true, moderator: true });
    }
    catch(err) {
      console.error(err);
    }
    try {
      fs.writeFileSync('secrets.json', JSON.stringify(await secret.getSecretObject(), null, 2));
      console.info('Passwords and secrets are saved in secrets.json file');
    }
    catch(err) {
      console.error(err);
    }
    if(config.xmpp.enable) {
      await initXmpp.init();
      for(var i = 0;i < config.xmpp.defaultRooms.length;i++) {
        await room.addRoom(config.xmpp.defaultRooms[i]);
      }
    }
  }
  catch(err) {
    console.error(err);
  }
};
