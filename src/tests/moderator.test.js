const app = require('../app');
const supertest = require('supertest');
const request = supertest(app);
const testSetup = require('./testSetup');
beforeAll(async () => { await testSetup.register(request); });

test('Test elevating user to moderator', async () => {
  let admin = await request.post('/login').send({
    username: 'admin',
    password: await testSetup.getAdminPass()
  });
  expect(admin.status).toBe(200);
  expect(typeof admin.text).toBe('string');
  let adminToken = admin.text;

  let user1 = await request.post('/login').send({
    username: 'user1',
    password: 'user1'
  });
  expect(user1.status).toBe(200);
  expect(typeof user1.text).toBe('string');
  let user1Token = user1.text;

  let res1 = await request.post('/users/user1/moderator').set('Authorization', adminToken).send({
    moderator: false
  });
  expect(res1.status).toBe(200);

  let res2 = await request.post('/users/user2/ban').set('Authorization', user1Token).send({
    banDate: false
  });
  expect(res2.status).toBe(403);
  expect(res2.body).toStrictEqual({
    error: 'Requesting user is not moderator or admin!'
  });

  let res3 = await request.post('/users/user1/moderator').set('Authorization', adminToken).send({
    moderator: true
  });
  expect(res3.status).toBe(200);

  let res4 = await request.post('/users/user2/ban').set('Authorization', user1Token).send({
    banDate: false
  });
  expect(res4.status).toBe(200);

  let res5 = await request.post('/users/user1/moderator').set('Authorization', adminToken).send({
    moderator: false
  });
  expect(res5.status).toBe(200);
});

test('Test elevating user to moderator from unauthorized user', async () => {
  let admin = await request.post('/login').send({
    username: 'admin',
    password: await testSetup.getAdminPass()
  });
  expect(admin.status).toBe(200);
  expect(typeof admin.text).toBe('string');
  let adminToken = admin.text;

  let user1 = await request.post('/login').send({
    username: 'user1',
    password: 'user1'
  });
  expect(user1.status).toBe(200);
  expect(typeof user1.text).toBe('string');
  let user1Token = user1.text;

  let res1 = await request.post('/users/user1/moderator').set('Authorization', adminToken).send({
    moderator: false
  });
  expect(res1.status).toBe(200);

  let res2 = await request.post('/users/user2/moderator').set('Authorization', user1Token).send({
    moderator: false
  });
  expect(res2.status).toBe(403);
  expect(res2.body).toStrictEqual({
    error: 'Requesting user is not moderator or admin!'
  });

  let res3 = await request.post('/users/user2/moderator').set('Authorization', user1Token).send({
    moderator: true
  });
  expect(res3.status).toBe(403);
  expect(res3.body).toStrictEqual({
    error: 'Requesting user is not moderator or admin!'
  });
});