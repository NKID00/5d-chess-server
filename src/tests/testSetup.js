const fs = require('fs');

exports.register = async (request) => {
  await request.get('/_init');
  await request.post('/register').send({
    username: 'user1',
    password: 'user1',
    bio: 'Test biography',
    fullname: 'User 1',
    country: 'USA'
  });
  await request.post('/register').send({
    username: 'user2',
    password: 'user2',
    bio: 'Test biography',
    fullname: 'User 2',
    country: 'USA'
  });
}

exports.getAdminPass = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      let secrets = JSON.parse(fs.readFileSync('./secrets.json'));
      resolve(secrets.adminpass);
    }, 2500);
  });
}