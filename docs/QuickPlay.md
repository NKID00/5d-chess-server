# WIP

## API

POST /quickplay/queue
 - Request:
    {
      ranked: true | false,
      variants: [string],
      formats: [string],
      minElo: number,
      maxElo: number,
    }
 - Response:
    {
      id: string[Queue Id]
      sessionId: null | string[Session Id]
    }

GET /quickplay/queue
 - Response:
    {
      id: string[Queue Id]
      sessionId: null | string[Session Id]
    }

GET /quickplay/cancel

GET /quickplay/confirm

GET /quickplay/info
 - Response:
    {
      totalPlayers: number,
      variants: [
        { variant: string, totalPlayers: number }
      ]
    }

## Backend Design

Database collections:
  - quickplay-users - contain users looking to play
  - quickplay-matches - contain quickplay matches in progress (session has not been created)

``` mermaid
graph TD
  queueRequest([POST /quickplay/queue])
  extractUser[Find user ELO info using overall]
  queueRequest-->extractUser
  createQueueTicket[Create queue ticket]
  extractUser-- If queue ticket does not exist -->createQueueTicket
  error500[Return status 500]
  extractUser-- If queue ticket exists -->error500
  searchMatch[Look for another compatible queue ticket]
  createQueueTicket-->searchMatch
  createSession[Create a session]
  searchMatch-- If match is found -->createSession
  addSessionToQueue[Add session id to queue tickets]
  createSession-->addSessionToQueue
  returnQueueTicket[Return queue ticket]
  addSessionToQueue-->returnQueueTicket
  searchMatch-- If match is not found -->returnQueueTicket

  queueInfo([GET or POST /quickplay/queue/:id])
  queueInfo-- If queue ticket does not exist -->error500
  updateQueueTicket[Update queue ticket]
  queueInfo-- If queue ticket exists -->updateQueueTicket
  updateQueueTicket-->searchMatch
  
  queueCancel([GET /quickplay/cancel])
  removeQueueTicket[Remove queue ticket]
  queueCancel-- If queue ticket exists -->removeQueueTicket
  removeSession[Remove session]
  removeQueueTicket-->removeSession

  queueConfirm([GET /quickplay/confirm])
  confirmQueueTicket[Update queue ticket]
  queueConfirm-- If queue ticket exists -->confirmQueueTicket
  startSession[Start session]
  confirmQueueTicket-->startSession
```