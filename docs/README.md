# Overview

Open source implementation of '5D Chess With Multiverse Time Travel' as a REST style API server with ELO rankings.

Official server instance is https://server.chessin5d.net.

# Installation

To run the server, clone this repo using `git clone https://gitlab.com/5d-chess/5d-chess-server`.

Run `cd 5d-chess-server` and then `npm install` to install dependencies.

Standard way of running the server would be to use `npm start`, however for some production environments, it may be beneficial to have the node process be restarted automatically on full crash. Running the command `npm run fstart` will use the [forever](https://www.npmjs.com/package/forever) npm package to keep the server running even on crashes.

# Config

In the default configuration, 5D Chess Server starts a local [NeDB](https://github.com/louischatriot/nedb) instance and runs on port 5000. To change this configuration, create a new file in the project root directory called `config.json` and copy and paste text inside `defaultConfig.json` into the `config.json` file.

The `config.json` file contains JSON data to be used as configuration settings. Here are the following fields available.

  - `db` - JSON Object or null, used to define what database to connect too. If null, it uses NeDB to create a local MongoDB compatible database instance.
    - `type` - String indicating what database driver to use (should be MongoDB compatible). Currently only 'mongodb' is implemented. Other fields with the sub-object may be different depending on this field.
    - If `type === 'mongodb'`, following fields exist:
      - `url` - MongoDB connection string (https://docs.mongodb.com/manual/reference/connection-string/).
      - `options` - JSON Object for MongoDB connection options (http://mongodb.github.io/node-mongodb-native/2.2/reference/connecting/connection-settings/).
  - `saltRounds` - Number indicating cost factor for bcrypt salting when hashing passwords. Higher is more secure, but will decrease performance. Default of 10 is used. More information available here (https://stackoverflow.com/questions/4443476/optimal-bcrypt-work-factor).
  - `heartbeat` - Number indicating the interval in milliseconds for the server heartbeat function to run. Default of 60000 (1 minute). This is used to run cleanup operations periodically.
  - `archive` - Number indicating the amount of time after a session has ended for the server to wait before removing it. Default of 300000 (5 minutes).
  - `startDeadline` - Number indicating the amount of time to wait for session to start before terminating it. Default of 300000 (5 minutes).
  - `deadline` - Number indicating the amount of time to wait for session to finish before terminating it (regardless if it has started or not). Default of 604800000 (1 week).
  - `startingElo` - Number indicating the starting ELO of new players
  - `enableBrute` - Boolean indicating whether to enable rate limiting functionality. Defaults to `false` for local testing purposes, but it should be `true` on production environments.
  - `enableTrustProxy` - Boolean to enable trust proxy (https://expressjs.com/en/guide/behind-proxies.html). Defaults to `false`.
  - `enableCors` - Boolean to enable CORS on all routes. Defaults to `true`.
  - `port` - Number indicating network port to run the server on. Default of 5000. If the `PORT` environment variable is set, it will use that value instead.
  - `gatewayString` - String to return when visiting the root endpoint.
  - `motdString` - String to use as the 'message of the day'.
  - `emailRecovery` - Subobject of email recovery related settings.
    - `enable` - Boolean indicating whether to enable the email recovery feature.
    - `address` - String with server's own email address.
    - `subject` - String indicating the subject text to use in email.
    - `recoverUrl` - URL for recovery link (will be appended by recovery code).
    - `title` - String indicating text for the title inside the email.
    - `timeout` - Number indicating how long the code is valid (in milliseconds). Defaults to `300000` (5 minutes).
  - `emailOptions` - Subobject of email related config (https://nodemailer.com/smtp/). Defaults to empty object. 
  - `xmpp` - Subobject of XMPP related settings.

    - `enable` - Boolean indicating whether to enable the XMPP feature.
    - `service` - URL to use for the XMPP connection.
    - `domain` - Domain to use for XMPP requests.
    - `muc` - Domain for the muc component.
    - `username` - JID of the admin account.
    - `password` - Password for the admin account.
    - `defaultRooms` - Array of strings indicating default rooms to create and maintain.

# API

For all routes unless otherwise specified, the data should be in the form of JSON data (`Content-Type` header should have the value of `application/json`).

For routes requiring user authorization, JWT is required for authentication. These tokens have a max lifetime of 12 hours. To authenticate, the `Authorization` header should have the value of the JWT string.

Example: 
``` js
{
  "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
}
```

Authentication failures will return HTTP status code of `401 Unauthorized` (no other route will return this status code).Authorization failures will return HTTP status code of `403 Forbidden`.

For routes that don't require user authorization, it's recommended to authenticate anyways to update the `lastAuth` field in the user data. This field is used to indicate if users are online or not.

On error, the server will return error data in the form of JSON data with the error field containing error information. This could either be a string or an object with `message` and `stack` fields.

Default user of 'admin' is created on startup, with full ability to do all actions. When displaying all users, it is advise to skip the admin user (will always have 'admin' username).

## Authentication

### **POST** /register

Endpoint used to register new users into the system. No JWT authentication needed.

By default, registering users via regular HTTP requests results in the user being labeled as a bot. This has no functional impact other than notifying other users that the user in question is most likely automated. To integrate with clients designed for end users, contact the server admin for more information regarding registering non-bot users. For others running new instances of this server, contact the author for more information regarding registering non-bot users. This system is very weak and is only meant to make bot/engine developers aware that they should register as a bot user.

  - **Rate Limit** - Allows one request every 5 seconds.

  - **Request** - JSON data containing the information for registering a user.

    - `username` - **[Required]** Username string containing only a-z, 0-9, _ and - characters. This must be unique (no two users can have the same username). Truncated to first 100 characters.
    - `password` - **[Required]** Password string used for login. Truncated to first 100 characters.
    - `avatar` - URL (can be image data url) pointing to image to be used as the user avatar (will be scaled and cropped to dimensions of 128 x 128 pixels).
    - `fullname` - String for full name of user. Truncated to the first 100 characters.
    - `email` - String for email of user. Used for password recovery.
    - `showEmail` - Boolean for indicating if email should be publically shown.
    - `bio` - String for user description. Truncated to the first 500 characters.
    - `country` - ISO 3166-1 Alpha-3 compliant string to indicate user's country of origin (List here: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3#Officially_assigned_code_elements).

  - **Response** - JWT for authentication in the form of a string.

### **POST** /login

Endpoint used to login from. No JWT authentication needed. Returns JWT for authentication purposes.

  - **Rate Limit** - Allows one request every 5 seconds.

  - **Request** - JSON data containing the information for logging in a user.

    - `username` - **[Required]** Username string containing only a-z, 0-9, _ and - characters. This must match existing user.
    - `password` - **[Required]** Password string used for login.

   - **Response** - JWT for authentication in the form of a string.

### **POST** /recover

Endpoint used to set new password and recover a user account (if email recovery is enabled). No JWT authentication needed.

  - **Rate Limit** - Allows one request every 5 seconds.

  - **Request** - JSON data containing the information for recovering a user account.

    - `email` - **[Required]** Email string matching the user account to recover.
    - `recoverCode` - **[Required]** Recovery code string matching the code sent via email.
    - `password` - **[Required]** Password string used for setting new password.

  - **Response** - No response body, check for `200 OK` HTTP status code for success.

### **GET** /refreshToken

Endpoint used to obtain new token via old token (skip login process and get new token before old token expires). Requires JWT authentication. Returns new JWT for authentication purposes.

  - **Rate Limit** - Allows one request every 5 seconds.

  - **Response** - JWT for authentication in the form of a string.

### **GET** /authCheck

Endpoint used to check if token is valid. Requires JWT authentication.

  - **Rate Limit** - Allows one request every 1 seconds.

  - **Response** - No response body, check for `200 OK` HTTP status code for validity.

### **POST** /recoverCode

Endpoint used to request a recovery code to recover a user account (if email recovery is enabled). No JWT authentication needed.

  - **Rate Limit** - Allows one request every 5 seconds.

  - **Request** - JSON data containing the information for recovering a user account.

    - `email` - **[Required]** Email string matching the user to recover.

  - **Response** - No response body, check for `200 OK` HTTP status code for success.

### **GET** /xmpp

Endpoint used to obtain XMPP login information (if XMPP is enabled). Requires JWT authentication.

  - **Rate Limit** - Allows one request every 1 seconds.

  - **Response** - JSON data containing XMPP login information.

    - `domain` - String containing the base domain of the XMPP server.
    - `muc` - String containing the domain of the muc (Multi User Chat) component.
    - `username` - String containing the full JID of the authenticated user.
    - `password` - String containing the XMPP password of the authenticated user.
    - `defaultRooms` - Array of strings containing full JID of default muc rooms (these should be almost always on).

## User

### **GET** /users/:username

Endpoint used to get a user's information. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Response** - JSON data containing user information.

    - `username` - Unique string identifier.
    - `avatar` - Data url of image data (content type of image/png) representing the avatar of the user.
    - `fullname` - String identifier meant for containing the full name of the user.
    - `email` - String for email of user. Used for password recovery. Only shown if authenticated as the user or showEmail is `true`.
    - `showEmail` - Boolean for indicating if email should be publically shown.
    - `bio` - String containing description of the user.
    - `country` - ISO 3166-1 Alpha-3 compliant string to indicate user's country of origin (List here: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3#Officially_assigned_code_elements).
    - `bot` - Boolean indicating if user is registered as a bot or not.
    - `joinDate` - UNIX epoch time in milliseconds indicating when the user was registered.
    - `banDate` - Boolean or number. If boolean, used to indicatre if a user is banned indefinitely. If a number, must be UNIX epoch time in milliseconds representing end date of ban.
    - `lastAuth` - UNIX epoch time in milliseconds indicating the last time the user authenticated (can be used to indicate if user is online, look for if lastAuth is within the last 10 seconds).
    - `admin` - Boolean indicating if user is an admin.
    - `moderator` - Boolean indicating if user is a moderator.

### **GET | POST** /users

Endpoint used to get multiple users. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Request** - For use with [POST /users](?id=get-post-users) request. Should be JSON data with the following fields.

    - `query` - MongoDB style query object (see https://docs.mongodb.com/manual/reference/method/db.collection.find/).
    - `projection` - MongoDB style projection object (see https://docs.mongodb.com/manual/reference/method/db.collection.find/).
    - `sort` - MongoDB style sort object (see https://docs.mongodb.com/manual/reference/method/cursor.sort/).
    - `limit` - MongoDB style limit number (see https://docs.mongodb.com/manual/reference/method/cursor.limit/).
    - `skip` - MongoDB style skip number (see https://docs.mongodb.com/manual/reference/method/cursor.skip/).

  - **Response** - JSON data containing array of user information. For each user, the user object contains the same fields as [GET /users/:username](?id=get-usersusername).

### **POST** /users/:username/update

Endpoint used to update user information. Requires JWT authentication. Only original user or admins can update.

  - **Rate Limit** - Allows one request every 5 seconds

  - **Request** - JSON data containing new user information to update.

    - `avatar` - URL (can be image data url) pointing to image to be used as the user avatar (will be scaled and cropped to dimensions of 128 x 128 pixels).
    - `fullname` - String identifier meant for containing the full name of the user. Truncated to the first 100 characters.
    - `email` - String for email of user. Used for password recovery.
    - `showEmail` - Boolean for indicating if email should be publically shown.
    - `bio` - String containing description of the user. Truncated to the first 500 characters.
    - `country` - ISO 3166-1 Alpha-3 compliant string to indicate user's country of origin (List here: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3#Officially_assigned_code_elements).

  - **Response** - JSON data containing updated user information (same as [GET /users/:username](?id=get-usersusername)).

## User

### **POST** /users/:username/ban

Endpoint used to ban a user. Requires JWT authentication. Requesting user must be moderator or admin.

  - **Rate Limit** - Allows one request every 5 seconds

  - **Request** - JSON data containing new user information to update.

    - `banDate` - Boolean or number. If boolean, used to unban user or ban a user indefinitely. If a number, must be UNIX epoch time in milliseconds representing end date of ban.

  - **Response** - JSON data containing updated user information (same as [GET /users/:username](?id=get-usersusername)).

### **POST** /users/:username/moderator

Endpoint used to promote or demote a user to moderator. Requires JWT authentication. Requesting user must be moderator or admin.

  - **Rate Limit** - Allows one request every 5 seconds

  - **Request** - JSON data containing new user information to update.

    - `moderator` - Boolean, used to set if a user is a moderator or not.

  - **Response** - JSON data containing updated user information (same as [GET /users/:username](?id=get-usersusername)).

### **POST** /users/:username/admin

Endpoint used to promote or demote a user to admin. Requires JWT authentication. Requesting user must be admin.

  - **Rate Limit** - Allows one request every 5 seconds

  - **Request** - JSON data containing new user information to update.

    - `admin` - Boolean, used to set if a user is a admin or not.

  - **Response** - JSON data containing updated user information (same as [GET /users/:username](?id=get-usersusername)).

## Session

Sessions are active games in play.

### **GET** /sessions/:id

Endpoint used to get session data via session id. No JWT authentication needed.

  - **Rate Limit** - Allows one request every 0.25 seconds.

  - **Response** - JSON data containing session data.

    - `id` - Unique string identifier.
    - `host` - Username of the user that created the session.
    - `white` - Username of the user/player playing white.
    - `black` - Username of the user/player playing black.
    - `variant` - String indicating which 5D Chess variant in play. Can be 5DPGN/5DFEN format.
    - `format` - String indicating timing format in use (Can be using [format notation](https://5d-chess.gitlab.io/5d-chess-clock/#/?id=format-notation) or [standardized format](https://5d-chess.gitlab.io/5d-chess-clock/#/?id=supported-timing-formats)).
    - `ranked` - Boolean indicating if session is ranked.
    - `ready` - Boolean indicating if non-host user/player is ready to play.
    - `requestJoin` - Array of usernames of users looking to join session.
    - `offerDraw` - Boolean indicating if current player is offering draw. Resets on submit.
    - `started` - Boolean indicating if session has started.
    - `startDate` - UNIX epoch time in milliseconds indicating when the session was started. If session has not started, this field indicates when the session was created.
    - `ended` - Boolean indicating if session has ended.
    - `endDate` - UNIX epoch time in milliseconds indicating when the session was ended.
    - `processing` - Boolean indicating if server is processing session (mainly to indicate server is doing checkmate detection), timed controls are paused during processing.
    - `archiveDate` - UNIX epoch time in milliseconds indicating when the session will be archived.
    - `timed` - Object containing timing information (`null` if game is not timed). Data is derived from the [internal state](https://5d-chess.gitlab.io/5d-chess-clock/#/?id=internal-state) of the 5d-chess-clock library.
    - `board` - `Board` object representing the current session (https://5d-chess.gitlab.io/5d-chess-js/#/?id=board-1).
    - `actionHistory` - Array of `Action` objects representing the history of actions in current session (https://5d-chess.gitlab.io/5d-chess-js/#/?id=action).
    - `moveBuffer` - Array of `Move` objects representing the moves played by the current player in the current session (https://5d-chess.gitlab.io/5d-chess-js/#/?id=move).
    - `player` - String indicating the current player ('white' or 'black').
    - `winner` - String indicating the winning player ('white', 'black', or 'draw'). Will be `null` if current session has not ended yet.
    - `winCause` - String indicating the reason for winning ('timed_out', 'forfeit', or 'regular'). Stalemates results in 'regular', whereas draws results in 'forfeit'. Will be `null` if current session has not ended yet.

### **GET | POST** /sessions

Endpoint used to get multiple sessions. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Request** - For use with [POST /sessions](?id=get-post-sessions) request. Should be JSON data with the following fields.

    - `query` - MongoDB style query object (see https://docs.mongodb.com/manual/reference/method/db.collection.find/).
    - `projection` - MongoDB style projection object (see https://docs.mongodb.com/manual/reference/method/db.collection.find/).
    - `sort` - MongoDB style sort object (see https://docs.mongodb.com/manual/reference/method/cursor.sort/).
    - `limit` - MongoDB style limit number (see https://docs.mongodb.com/manual/reference/method/cursor.limit/).
    - `skip` - MongoDB style skip number (see https://docs.mongodb.com/manual/reference/method/cursor.skip/).

  - **Response** - JSON data containing array session object data. For each session, the session object contains the same fields as [GET /sessions/:id](?id=get-sessionsid).

### **POST** /sessions/new

Endpoint used to create a new session. Requires JWT authentication.

  - **Rate Limit** - Allows one request every second.

  - **Request** - JSON data containing session information to create a new session.

    - `player` - String indicating the side the current user is going to play ('white', 'black', or 'random'). Defaults to 'white'.
    - `variant` - String indicating which 5D Chess variant to play (defaults to 'standard'). Can be 5DPGN/5DFEN format.
    - `format` - String indicating timing format in use (Can be using [format notation](https://5d-chess.gitlab.io/5d-chess-clock/#/?id=format-notation) or [standardized format](https://5d-chess.gitlab.io/5d-chess-clock/#/?id=supported-timing-formats)).
    - `ranked` - Boolean indicating if session is ranked.

  - **Response** - JSON data containing session data (same as [GET /sessions/:id](?id=get-sessionsid)).

### **POST** /sessions/:id/update

Endpoint used to update a session via session id. Requires JWT authentication. Must be host and non-host player has not signal ready.

  - **Rate Limit** - Allows one request every second.

  - **Request** - JSON data containing session information to create a new session.

    - `player` - String indicating the side the current user is going to play ('white', 'black', or 'random').
    - `variant` - String indicating which 5D Chess variant to play (defaults to 'standard'). Can be 5DPGN/5DFEN format.
    - `format` - String indicating timing format in use (Can be using [format notation](https://5d-chess.gitlab.io/5d-chess-clock/#/?id=format-notation) or [standardized format](https://5d-chess.gitlab.io/5d-chess-clock/#/?id=supported-timing-formats)).
    - `ranked` - Boolean indicating if session is ranked.

  - **Response** - JSON data containing session data (same as [GET /sessions/:id](?id=get-sessionsid)).

### **POST** /sessions/:id/remove

Endpoint used to remove a session via session id. Requires JWT authentication. Must be host.

  - **Rate Limit** - Allows one request every second.

  - **Request** - No data required.

  - **Response** - No data returned.

### **POST** /sessions/:id/requestJoin

Endpoint used to signal that the user is requesting to be non-host player and join the session via session id. Requires JWT authentication.

  - **Rate Limit** - Allows one request every second.

  - **Request** - No data required.

  - **Response** - JSON data containing session data (same as [GET /sessions/:id](?id=get-sessionsid)).

### **POST** /sessions/:id/addUser

Endpoint used to add a user as a player to a session data via session id. Requires JWT authentication. Must be host.

  - **Rate Limit** - Allows one request every second.

  - **Request** - JSON data containing user to add as second player

    - `username` - **[Required]** Username string of the user to add as second player.

  - **Response** - JSON data containing session data (same as [GET /sessions/:id](?id=get-sessionsid)).

### **POST** /sessions/:id/ready

Endpoint used to signal that the non-host player is ready to play in a session a via session id. Requires JWT authentication. Must be non-host player.

  - **Rate Limit** - Allows one request every second.

  - **Request** - No data required.

  - **Response** - JSON data containing session data (same as [GET /sessions/:id](?id=get-sessionsid)).

### **POST** /sessions/:id/unready

Endpoint used to signal that the non-host player is not ready to play in a session a via session id. Requires JWT authentication. Must be non-host player.

  - **Rate Limit** - Allows one request every second.

  - **Request** - No data required.

  - **Response** - JSON data containing session data (same as [GET /sessions/:id](?id=get-sessionsid)).

### **POST** /sessions/:id/start

Endpoint used to signal that the session has been started via session id. Requires JWT authentication. Must be host player.

  - **Rate Limit** - Allows one request every second.

  - **Request** - No data required.

  - **Response** - JSON data containing session data (same as [GET /sessions/:id](?id=get-sessionsid)).

### **POST** /sessions/:id/move

Endpoint used to make a move in an active session. Requires JWT authentication. User must be current player.

  - **Rate Limit** - Allows one request every 0.25 seconds.

  - **Request** - **[Required]** JSON of a valid `Move` object (https://5d-chess.gitlab.io/5d-chess-js/#/?id=move).

  - **Response** - JSON data containing session data (same as [GET /sessions/:id](?id=get-sessionsid)).

### **POST** /sessions/:id/undo

Endpoint used to undo a move in an active session. Requires JWT authentication. User must be current player.

  - **Rate Limit** - Allows one request every 0.25 seconds.

  - **Request** - No data required.

  - **Response** - JSON data containing session data (same as [GET /sessions/:id](?id=get-sessionsid)).

### **POST** /sessions/:id/submit

Endpoint used to submit all moves played in moveBuffer in an active session. Requires JWT authentication. User must be current player.

  - **Rate Limit** - Allows one request every 0.25 seconds.

  - **Request** - No data required.

  - **Response** - JSON data containing session data (same as [GET /sessions/:id](?id=get-sessionsid)).

### **POST** /sessions/:id/forfeit

Endpoint used to forfeit in an active session. Requires JWT authentication. User must be current player.

  - **Rate Limit** - Allows one request every second.

  - **Request** - No data required.

  - **Response** - JSON data containing session data (same as [GET /sessions/:id](?id=get-sessionsid)).

### **POST** /sessions/:id/draw

Endpoint used to either offer or accept a draw in an active session. Requires JWT authentication. If user is current player, they offer a draw. If user is not current player, they accept draw request.

  - **Rate Limit** - Allows one request every second.

  - **Request** - No data required.

  - **Response** - JSON data containing session data (same as [GET /sessions/:id](?id=get-sessionsid)).

## Game

Games are previous sessions that have been played.

### **GET** /games/:id

Endpoint used to get game data via game id. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Response** - JSON data containing game data.

    - `id` - Unique string identifier.
    - `sessionId` - Unique string identifier representing the previous session this game was created from.
    - `white` - Username of the user/player playing white.
    - `black` - Username of the user/player playing black.
    - `variant` - String indicating which 5D Chess variant was played.
    - `format` - String indicating timing format used.
    - `ranked` - Boolean indicating if game is ranked.
    - `startDate` - UNIX epoch time in milliseconds indicating when the game was started.
    - `endDate` - UNIX epoch time in milliseconds indicating when the game was ended.
    - `actionHistory` - Array of `Action` objects representing the history of actions in the game (https://5d-chess.gitlab.io/5d-chess-js/#/?id=action).
    - `winner` - String indicating the winning player ('white', 'black', or 'draw').
    - `winCause` - String indicating the reason for winning ('timed_out', 'forfeit', or 'regular'). Stalemates results in 'regular', whereas draws results in 'forfeit'.

### **GET | POST** /games

Endpoint used to get multiple games. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Request** - For use with [POST /games](?id=get-post-games) request. Should be JSON data with the following fields.

    - `query` - MongoDB style query object (see https://docs.mongodb.com/manual/reference/method/db.collection.find/).
    - `projection` - MongoDB style projection object (see https://docs.mongodb.com/manual/reference/method/db.collection.find/).
    - `sort` - MongoDB style sort object (see https://docs.mongodb.com/manual/reference/method/cursor.sort/).
    - `limit` - MongoDB style limit number (see https://docs.mongodb.com/manual/reference/method/cursor.limit/).
    - `skip` - MongoDB style skip number (see https://docs.mongodb.com/manual/reference/method/cursor.skip/).

  - **Response** - JSON data containing array game object data. For each game, the game object contains the same fields as [GET /games/:id](?id=get-gamesid).

## Ranking

Rankings are the calculated ELO ranking of a user from a ranked game. Multiple ranking objects of a user makes a history of how the user did in ranked play.

Note, not all users have rankings. Rankings only get created when user plays first ranked game. When users play their first ranked game, a 'starting' ranking with the starting ELO rating (default 1200) is created. This 'starting' ranking uses user join date as the date.

### **GET** /rankings/:id

Endpoint used to get ranking data via ranking id. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Response** - JSON data containing ranking data.

    - `id` - Unique string identifier.
    - `username` - Username of the user.
    - `variant` - String indicating which 5D Chess variant was played. Valid values are 'standard', 'defended pawn', 'half reflected', 'princess' or 'turn zero'.
    - `rating` - ELO rating of player calculated from starting game. Default starting ELO is 1200.
    - `format` - String indicating timing format used.
    - `date` - UNIX epoch time in milliseconds indicating when the ranking was calculated was calculated.
    - `gameId` - Corresponding id of the game that created this calculation. Empty string if ranking is the starting ELO.

### **GET | POST** /rankings

Endpoint used to get multiple rankings. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Request** - For use with [POST /rankings](?id=get-post-rankings) request. Should be JSON data with the following fields.

    - `query` - MongoDB style query object (see https://docs.mongodb.com/manual/reference/method/db.collection.find/).
    - `projection` - MongoDB style projection object (see https://docs.mongodb.com/manual/reference/method/db.collection.find/).
    - `sort` - MongoDB style sort object (see https://docs.mongodb.com/manual/reference/method/cursor.sort/).
    - `limit` - MongoDB style limit number (see https://docs.mongodb.com/manual/reference/method/cursor.limit/).
    - `skip` - MongoDB style skip number (see https://docs.mongodb.com/manual/reference/method/cursor.skip/).

  - **Response** - JSON data containing array ranking object data. For each ranking, the ranking object contains the same fields as [GET /rankings/:id](?id=get-rankingsid).

## Ranked

Endpoint used to check which board variants and time control formats are available for ranked play. When creating sessions for ranked play, the `variant` and `format` field should exactly match one of the options returned by the following format. These are the `shortName` values given by [.variants](https://5d-chess.gitlab.io/5d-chess-js/#/?id=variants) and [.formats](https://5d-chess.gitlab.io/5d-chess-clock/#/?id=formats).

### **GET** /ranked/variants

Endpoint used to get valid board variants for ranked play. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Response** - JSON array containing valid strings for the `variant` field (for ranked play).

### **GET** /ranked/formats

Endpoint used to get valid time control formats for ranked play. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Response** - JSON array containing valid strings for the `format` field (for ranked play).

## Quick Play

Endpoint for server side match making system. Using this system will trigger starting ELO rating calculations. Users marked as bot are not allowed to enter quick play queue.

### **GET** /quickplay

Endpoint used to query queue statistics. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Response** - JSON data containing queue statistics.

    - `totalPlayers` - Total number of players in queue.
    - `ranked` - Total number of players in queue looking for ranked play.
    - `variants` - Array of objects containing per variant statistics.
      - `name` - String indicating variant.
      - `totalPlayers` - Total number of players looking to play this variant.
      - `ranked` - Total number of players in queue looking for ranked play (this variant).
    - `formats` - Array of objects containing per format statistics.
      - `name` - String indicating format.
      - `totalPlayers` - Total number of players looking to play this format.
      - `ranked` - Total number of players in queue looking for ranked play (this format).


### **POST** /quickplay/queue

Endpoint used to enter quick play queue. Requires JWT authentication. Can only enter queue if not already in queue.

  - **Rate Limit** - Allows one request every second.

  - **Request** - JSON data for quick play parameters

    - `ranked` - **[Required]** Boolean indicating if looking for a ranked session
    - `variants` - **[Required]** Array of strings indicating which variants the user is looking to play. Must be variants available for ranked play (regardless of if queuing for ranked play or not).
    - `formats` - **[Required]** Array of strings indicating which time control formats the user is looking to play. Must be formats available for ranked play (regardless of if queuing for ranked play or not).
    - `minRating` - Number indicating the minimum ELO rating of the opponent to be matched as.
    - `maxRating` - Number indicating the maximum ELO rating of the opponent to be matched as.

  - **Response** - JSON data containing possible session id (if a match has been found) and date of when user entered queue.

    - `date` - UNIX epoch time in milliseconds indicating when user entered queue.
    - `sessionId` - String or `null`. If string, indicates a match has been found and a session has been created (string is id of said session).

### **GET** /quickplay/queue

Endpoint used to query queue status. Requires JWT authentication. Only works if already in queue.

  - **Rate Limit** - Allows one request every 0.25 seconds.

  - **Response** - JSON data containing possible session id (if a match has been found) and date of when user entered queue.

    - `date` - UNIX epoch time in milliseconds indicating when user entered queue.
    - `sessionId` - String or `null`. If string, indicates a match has been found and a session has been created (string is id of said session).

### **GET** /quickplay/cancel

Endpoint used to exit queue. Requires JWT authentication. Cannot exit queue if already confirmed (if other player does not confirm within 5 seconds, cancel is allowed).

  - **Rate Limit** - Allows one request every second.

  - **Response** - No response body, check for 200 OK HTTP status code for success.

### **GET** /quickplay/confirm

Endpoint used to confirm user has received session id. Requires JWT authentication.

  - **Rate Limit** - Allows one request every second.

  - **Response** - No response body, check for 200 OK HTTP status code for success.

# Example Client

Bottom example client uses [superagent](https://www.npmjs.com/package/superagent) as the AJAX HTTP request library.

``` js
const request = require('superagent');
const url = 'https://example.5dchessserver.net'; //Not real url

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds));
};

const main = async () => {
	//Registering test users
  try {
    await request.post(url + '/register').send({
      username: 'testuser1',
      password: 'testuser1',
      bio: 'Test User 1, not a real player',
      fullname: 'Test User 1'
    });
  }
  catch(err) {}
  await sleep(10000);
  try {
    await request.post(url + '/register').send({
      username: 'testuser2',
      password: 'testuser2',
      bio: 'Test User 2, not a real player',
      fullname: 'Test User 2'
    });
  }
  catch(err) {}
  await sleep(10000);
  
  //Login as test users and grab JWT
	var user1 = await request.post(url + '/login').send({
    username: 'testuser1',
    password: 'testuser1'
  });
  var user1Token = user1.text;
  console.log('Test User 1 JWT: ' + user1Token);
  await sleep(10000);
  var user2 = await request.post(url + '/login').send({
    username: 'testuser2',
    password: 'testuser2'
  });
  var user2Token = user2.text;
  console.log('Test User 2 JWT: ' + user2Token);
  await sleep(10000);
  
  //Testuser1 creates session
  var res = await request.post(url + '/sessions/new').set('Authorization', user1Token).send({
    player: 'white'
  });
  var ses = res.body;
  console.log('Session created:');
  console.log(JSON.stringify(ses, null, 2));
  await sleep(1000);
  
  //Testuser1 adds testuser2 as the other player
  res = await request.post(url + '/sessions/' + ses.id + '/addUser').set('Authorization', user1Token).send({
    username: 'testuser2'
  });
  await sleep(1000);
  
  //Testuser2 signals ready for game to start
  res = await request.post(url + '/sessions/' + ses.id + '/ready').set('Authorization', user2Token).send({});
  await sleep(1000);
  
  //Testuser1 starts game
  res = await request.post(url + '/sessions/' + ses.id + '/start').set('Authorization', user1Token).send({});
  ses = res.body;
  console.log('Session started:');
  console.log(JSON.stringify(ses, null, 2));
  await sleep(1000);
  
  //Testuser1 plays first action
  res = await request.post(url + '/sessions/' + ses.id + '/move').set('Authorization', user1Token).send({
    "promotion": null,
    "enPassant": null,
    "castling": null,
    "start": {
      "timeline": 0,
      "turn": 1,
      "player": "white",
      "coordinate": "e2",
      "rank": 2,
      "file": 5
    },
    "end": {
      "timeline": 0,
      "turn": 1,
      "player": "white",
      "coordinate": "e3",
      "rank": 3,
      "file": 5
    },
    "player": "white"
  });
  await sleep(1000);
  res = await request.post(url + '/sessions/' + ses.id + '/submit').set('Authorization', user1Token).send({});
  await sleep(1000);
  
  //User2 plays second action
  res = await request.post(url + '/sessions/' + ses.id + '/move').set('Authorization', user2Token).send({
    "promotion": null,
    "enPassant": null,
    "castling": null,
    "start": {
      "timeline": 0,
      "turn": 1,
      "player": "black",
      "coordinate": "f7",
      "rank": 7,
      "file": 6
    },
    "end": {
      "timeline": 0,
      "turn": 1,
      "player": "black",
      "coordinate": "f6",
      "rank": 6,
      "file": 6
    },
    "player": "black"
  });
  await sleep(1000);
  res = await request.post(url + '/sessions/' + ses.id + '/submit').set('Authorization', user2Token).send({});
  await sleep(1000);
  
  //User1 plays third action
  res = await request.post(url + '/sessions/' + ses.id + '/move').set('Authorization', user1Token).send({"promotion":null,"enPassant":null,"castling":null,"start":{"timeline":0,"turn":2,"player":"white","coordinate":"d1","rank":1,"file":4},"end":{"timeline":0,"turn":2,"player":"white","coordinate":"f3","rank":3,"file":6},"player":"white"});
  await sleep(1000);
  res = await request.post(url + '/sessions/' + ses.id + '/submit').set('Authorization', user1Token).send({});
  await sleep(1000);
  
  //User2 plays four action
  res = await request.post(url + '/sessions/' + ses.id + '/move').set('Authorization', user2Token).send({"promotion":null,"enPassant":null,"castling":null,"start":{"timeline":0,"turn":2,"player":"black","coordinate":"d7","rank":7,"file":4},"end":{"timeline":0,"turn":2,"player":"black","coordinate":"d6","rank":6,"file":4},"player":"black"});
  await sleep(1000);
  res = await request.post(url + '/sessions/' + ses.id + '/submit').set('Authorization', user2Token).send({});
  await sleep(1000);
  
  //User1 plays fifth action
  res = await request.post(url + '/sessions/' + ses.id + '/move').set('Authorization', user1Token).send({"promotion":null,"enPassant":null,"castling":null,"start":{"timeline":0,"turn":3,"player":"white","coordinate":"f3","rank":3,"file":6},"end":{"timeline":0,"turn":3,"player":"white","coordinate":"h5","rank":5,"file":8},"player":"white"});
  await sleep(1000);
  res = await request.post(url + '/sessions/' + ses.id + '/submit').set('Authorization', user1Token).send({});
  ses = res.body;
  console.log('Session ended:');
  console.log(JSON.stringify(ses, null, 2));
  await sleep(1000);
};

main();
```

Live demo on JSFiddle available [here](https://jsfiddle.net/alexbay218/0hpo4wbL/)

# FAQ

## Is it any good?

Yes (maybe).

## You incorrectly evaluated this full board as a checkmate (or not a checkmate)!

If you can provide me an action list (object, json, or notation) or the board state, and submit it as an issue to this repo (https://gitlab.com/5d-chess/5d-chess-js), I can get right on it. This goes for any other bugs. A good way to verify if it is correct or not is to repeat the same moves in the same order in '5D Chess With Multiverse Time Travel' and see if it matches.

## Why is this on GitLab instead of GitHub?

I made the switch from GitHub to GitLab mid 2019 when I was starting a new long term project called KSS. Back then, GitHub did not have many of the features it does now, such as integrated CI/CD and more. GitLab was the superior product in almost every way. Furthermore, as a believer in the open source, it seem ironic that open source software would be hosted on closed source platforms. With GitLab being open source, I can be sure that if GitLab.org crumbles, I can still maintain the overall project structure via GitLab instances. This allows me to preserve the Git repo itself, but also the issues, labels, rules, pipelines, etc. that are fundamental to a project. With GitHub, developers do not have this guarantee and they also do not have full control over their project structure.

For a (biased, but not untrue) comparison, visit this link [here](https://about.gitlab.com/devops-tools/github/decision-kit.html)

## Isn't the game copyrighted?

Yes, the game '5D Chess With Multiverse Time Travel' is under copyright by Thunkspace, LLC and any source code, written works, and other copyrightable materials are the property of Thunkspace, LLC. However, copyright does not extend to an idea, which include game rules. So as long as the new work does not contain a direct copy of the rules or other material within the original game. Well known precedent for this is Hasbro's lawsuit against Scrabulous in which they dropped it after Scrabulous removed material that could possible be considered violating copyright (https://www.cnet.com/news/hasbro-drops-scrabulous-lawsuit/).

Also of note is this article from the American Bar Association (https://www.americanbar.org/groups/intellectual_property_law/publications/landslide/2014-15/march-april/its_how_you_play_game_why_videogame_rules_are_not_expression_protected_copyright_law/).

5D Chess Server in no way aims to violate any copyright laws, but instead aims to be an open source implementation of the original ideas as presented by '5D Chess With Multiverse Time Travel'.

# Copyright

All source code is released under AGPL v3.0 (license can be found under the `LICENSE` file).

Any addition copyrightable material not covered under AGPL v3.0 is released under CC BY-SA v3.0.

*Note: For `defaultConfig.json`, since the software cannot be run without the file, it is considered "corresponding source" in section 1 in the AGPL v3.0. This means any modification is subject to the AGPL v3.0 license. For `config.json`, that file is not considered "corresponding source" (in the eyes of the author), so any modification is not subject to the AGPL v3.0 license. Please use `config.json` instead of `defaultConfig.json` in production environments to protect private secrets.*
