# 5D Chess Server

> Open source implementation of '5D Chess With Multiverse Time Travel' as a REST style API server with ELO rankings.

[GitLab](https://gitlab.com/5d-chess/5d-chess-server)
[Get Started](#overview)

![color](#000000)